import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Signup } from '../model/signup'
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(
    private http: HttpClient
  ) { }

  createNewUser(userData: Signup ): Observable<any> {
    // let configUrl = ``;
    // return this.http.post(configUrl, userData);
    let obj = {
      "msg": "registration done",
      "status": 200,
      "data": userData
    }
    return of(obj);
  }
}
