import { Component, OnInit} from '@angular/core';
import { Validators} from '@angular/forms';
import { Signup } from '../model/signup'
import { FormBuilder } from '@angular/forms';
import { SignUpService } from '../services/sign-up.service'

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  submitted: boolean = false;
  userDatabase: Signup[] = [];
  passwordNotMatched: boolean = false;

  signupForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    emailId: ['', [Validators.required,Validators.email]],
    password: ['', Validators.minLength(8)],
    confirmPassword: ['', Validators.minLength(8)]
  })

  constructor(
    private fb: FormBuilder,
    private signUpApi: SignUpService) {}

  ngOnInit(): void {}
  
  onSubmit() {
    this.submitted = true;
    if (this.signupForm.invalid) {
      return;
    }
    if (!this.passwordMatched()) {
      this.passwordNotMatched = true;
      return;
    }
    this.createNewUser()
  }

  createNewUser(){
    this.signUpApi.createNewUser(this.signupForm.value).subscribe((data: any) => {
      console.log(data);
    });
    this.signupForm.reset();
    this.submitted = false;
    this.passwordNotMatched = false;
  }

  resetSignupForm() {
    this.signupForm.reset();
    this.submitted = false;
  }

  get signup() {
    return this.signupForm.controls;
  }

  passwordMatched(){
    return this.signupForm.value.password === this.signupForm.value.confirmPassword
  }


}
